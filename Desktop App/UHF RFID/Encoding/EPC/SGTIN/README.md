# SGTIN 96/198 EPC Encoding

Contact help@gototags.com if you have any questions about retail mandate encoding file generation.

## Converting a UPC to SGTIN-96 Inputs

### Determining the Inputs

Example UPC: 073149097330

| Number System | Company Prefix | Item Number | Check Digit |
|---------------|----------------|-------------|-------------|
| 0             | 73149          | 09733       | 0           |

The first n digits of the UPC is the `Company Prefix` and the remaining are n digits of `Item Number`, with the last digit being the `Check Digit`.

You will need to know the company prefix and/or the item number in order to produce accurate encoding files. 

Note the values in the UPC do not directly map to the required values for the SGTIN-96.

For this example UPC, the company prefix within the UPC is `073149` and the item number is `09733`.

#### Separate the `UPC` into the `GS1 Company Prefix` and the `Item Reference Number`

1. Produce the GS1 Company Prefix by combining the UPC's `Number System` and `Company Prefix` and appending a 0 to the front
    - `0073149`
2. Produce the Item Reference Number by calculating the `Partition Value` for this EPC. 
   - Locate the table row on the `Partition Value Table` below that corresponds to the number of digits in the produced GS1 Company Prefix
   - This will tell you the number of digits the Item Ref should be
   - This value should be larger than or equal to the number of digits remaining in the UPC between the company prefix and the check digit
   - If equal, you can just use it. If larger, you need to append 0's (the difference between number indicated for the partition value and the actual number of digits pulled from the UPC).
   - If you know the item number within the UPC but not the company prefix, you can reverse this logic to determine how many leading digits of the UPC make up the company prefix

| Partition Value | GS1 Company Prefix Digit Count | Item Ref Digit Count |
|-----------------|--------------------------------|----------------------|
| 0               | 12                             | 1                    |
| 1               | 11                             | 2                    |
| 2               | 10                             | 3                    |
| 3               | 9                              | 4                    |
| 4               | 8                              | 5                    |
| 5               | 7                              | 6                    |
| 6               | 6                              | 7                    |

3. Remember the partition value previously calculated
   - `5`
4. Determine the EPC Filter Value:
   - `1`
   
| Filter Value | Description            |
|--------------|------------------------|
| 0            | All others             |
| 1            | Point of Sale          |
| 2            | Full Case Transport    |
| 3            | Inner Pack Trade Item  |
| 4            | Unit Load              |
| 5            | Unit Inside Trade Item |

### Constructing an Encoding Record

Use the values determined as inputs for the required SGTIN-96 inputs of a UHF Encode Tags `Tags` file. 

*- This example data set does not include all of the encoding file columns, only those pertaining to the SGTIN-96 EPC type.* 

*- Leading 0's do not need to be included in any encoding CSV value.*

| epc.type | epc.bitCount | epc.companyPrefix | epc.filterValue | epc.itemRefAndIndicator | epc.partitionValue | epc.serialNumberDecimal | additionalEncodingFileColumns |
|----------|--------------|-------------------|-----------------|-------------------------|--------------------|-------------------------|-------------------------------|
| SGTIN    | 96           | 73149             | 1               | 9733                    | 5                  | 0-274,877,906,943       | ...                           |