# NTAG21356 Mirroring

### Mirror Types
- NONE - No mirroring
- UID - Mirrors the UID
- COUNTER - Mirrors the counter
- UID_COUNTER - Mirrors the UID in that order separated by an 'x'