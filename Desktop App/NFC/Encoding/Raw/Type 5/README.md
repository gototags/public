# Type 5 Raw Encoding

This encoding example will write a 1 byte value to the final byte of block 1 for a type 5 NFC tag and will erase the rest of the tag. To write data to a specific memory block, fill in 0's until the desired block is reached.

Raw encoding is not reccomended for most users and can permanantly corrupt tags. The GoToTags Desktop App may no longer be able to read a tag that has been encoded with raw data.

