# Type 2 Raw Encoding

This encoding example will write a value to the final byte of block 4 for a type 2 NFC tag and will write the values 0001-000A to the tag's OTP while erasing the rest of the tag. To write data to a specific memory block, fill in 0's until the desired block is reached.

Raw encoding is not reccomended for most users and can permanantly corrupt tags. The GoToTags Desktop App may no longer be able to read a tag that has been encoded with raw data.

