# NTAG424 Encoding

NTAG424 encoding properties can be defined at either the operation level to apply to all tags, or at the tag level to apply to individual tags