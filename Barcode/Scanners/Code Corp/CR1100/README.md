# Code CR1100 

Technical documents, drivers and tools for the Code CR1100 barcode scanner.

- [Buy Code CR1100](https://store.gototags.com/catalogsearch/advanced/result/?sku=JXRJNE3UBM)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![Code CR1100](code_cr1100.jpg)