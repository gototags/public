# Artwork Templates

Most tags will require the use of Rectangle, Square, or Circle artwork templates.  Cards, FOB's, and Wristbands are some examples of tags that require special templates.  If you cannot find the template for the size of tag you are looking for [contact us](https://gototags.com/contact).

To learn more about artwork templates or printing tags in general visit our [learn site](https://learn.gototags.com/store/printing/artwork).