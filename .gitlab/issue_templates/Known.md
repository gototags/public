<!--
README

- Replace the comments below with actual text
- This is a Markdown file; see (https://www.markdownguide.org/basic-syntax/) for syntax
-->


<!-- A short description of the issue -->

### Environment

<!--
Include only what is relevant; delete what is not

- Software Version: *v1.2.3.4*
- Operating System: *Windows, Mac, Linux...*
- Browser: *Chrome, Firefox, ...*
- Hardware: *ACR122U, CIR215A, ...*
- Tags: *NTAG213, SLIX, ...*
- Data: *Website NDEF Record: `example.com`*
- *...*
-->

### Steps to Reproduce

<!--
The minimum and exact steps to reproduce the bug

1. *step 1*
2. *step 2*
-->

### Reason

<!-- Why is this issue happening? What is the reason behind for how it currently works? -->

### External Links

<!--
Links to relevant external resources (articles, data sheets, GitHub, StackOverflow...)

* http://example.com
-->

### Other

<!-- Anything else? (screenshots, files, logs...) -->

### Incidents

<!-- 
Associated incident ids (used to correlate incident id to issues when searching)

- *`1234567890`*
-->





<!--
TEMPLATE
This template is kept in the Engineering project; only edit templates in that folder. Commit changes there and copy to the main branch of all projects.
https://gitlab.com/GoToTags/Engineering/blob/master/.gitlab/issue_templates/
-->