# Identiv uTrust 3700F

Technical documents, drivers, firmware and tools for the Identiv uTrust 3700F NFC reader.

- [Learn about Identiv uTrust 3700F](https://learn.gototags.com/nfc/hardware/desktop/utrust-3700f)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)
