# Identiv uTrust 3720F HF

Technical documents, drivers, firmware and tools for the Identiv uTrust 3720F HF NFC reader.

- [Learn about Identiv uTrust 3720F HF](https://learn.gototags.com/nfc/hardware/desktop/utrust-3720f-hf)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)
