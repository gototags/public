# ACS ACR1552U

Technical documents, drivers and tools for the ACS ACR1552U NFC reader.

- [Buy ACS ACR1552U](https://store.gototags.com/catalogsearch/advanced/result/?sku=23GZVUL555)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACR1552U](acs_acr1552u.jpg)