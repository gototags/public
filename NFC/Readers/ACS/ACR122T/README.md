# ACS ACR122T

Technical documents, drivers and tools for the ACS ACR122T NFC reader.

- [Buy ACS ACR122T](https://store.gototags.com/catalogsearch/advanced/result/?sku=LPKYNWRJ8F)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACR122T](acs_acr122t.jpg)