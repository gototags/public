# ACS ACM1252U-Z6

Technical documents, drivers and tools for the ACS ACM1252U-Z6 NFC reader module.

- [Buy ACS ACM1252U-Z6](https://store.gototags.com/catalogsearch/advanced/result/?sku=4AMUNG4CYC)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACM1252U-Z6](acs_acm1252u-z6.jpg)