# ACS ACR122U

Technical documents, drivers and tools for the ACS ACR122U NFC reader.

- [Buy ACS ACR122U](https://store.gototags.com/catalogsearch/advanced/result/?sku=XW2AJ8EMJM)
- [Learn about ACS ACR122U](https://learn.gototags.com/nfc/hardware/desktop/acr122u)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACR122U](acs_acr122u.jpg)