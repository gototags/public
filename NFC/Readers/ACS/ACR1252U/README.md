# ACS ACR1252U

Technical documents, drivers and tools for the ACS ACR1252U NFC reader.

- [Buy ACS ACR1252U](https://store.gototags.com/catalogsearch/advanced/result/?sku=SSFEKLSJA3)
- [Learn about ACS ACR1252U](https://learn.gototags.com/nfc/hardware/desktop/acr1252u)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACR1252U](acs_acr1252u.jpg)