# ACS ACM1252U-Y3

Technical documents, drivers and tools for the ACS ACM1252U-Y3 NFC reader module.

- [Buy ACS ACM1252U-Y3](https://store.gototags.com/catalogsearch/advanced/result/?sku=B3QYDDCB9J)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACM1252U-Y3](acs_acm1252u-y3.jpg)