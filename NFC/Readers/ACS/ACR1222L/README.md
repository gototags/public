# ACS ACR1222L

Technical documents, drivers and tools for the ACS ACR1222L NFC reader.

- [Buy ACS ACR1222L](https://store.gototags.com/catalogsearch/advanced/result/?sku=Z2UBCDK9P2)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![ACS ACR1222L](acs_acr1222l.jpg)