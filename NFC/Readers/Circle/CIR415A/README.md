# Circle CIR415A

Technical documents, drivers and tools for the Circle CIR415A NFC reader.

- [Buy Circle CIR415A](https://store.gototags.com/catalogsearch/advanced/result/?sku=8ZQMMPDDBZ)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![Circle CIR415A](circle_cir415a.jpg)