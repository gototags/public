# Impinj R500 

Technical documents, drivers and tools for the Impinj R500 UHF RFID reader.

- [Buy Impinj R500](https://store.gototags.com/catalogsearch/advanced/result/?sku=WWPFLS9TZV)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![Impinj R500](impinj_r500.jpg)