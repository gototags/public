# GoToTags External E710

Technical documents, drivers and tools for the GoToTags External E710 UHF RFID Reader.

- [Buy GoToTags External E710 One Antenna Port](https://store.gototags.com/catalogsearch/advanced/result/?sku=XTUCBCSAPZ)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![GoToTags External E710 One Antenna Port](../gototags_external_uhf_rfid_usb_reader_one_antenna_port.jpg)