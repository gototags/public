# GoToTags External E310

Technical documents, drivers and tools for the GoToTags External E310 UHF RFID Reader.

- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![GoToTags External E310 One Antenna Port](../gototags_external_uhf_rfid_usb_reader_one_antenna_port.jpg)
