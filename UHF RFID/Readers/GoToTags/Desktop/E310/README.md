# GoToTags Desktop E310

Technical documents, drivers and tools for the GoToTags Desktop E310 UHF RFID Reader.

- [Buy GoToTags Desktop E310](https://store.gototags.com/catalogsearch/advanced/result/?sku=TDLP3LCFPP)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)

![GoToTags Desktop E310](gototags_e310_uhf_rfid_desktop_usb_reader.jpg)