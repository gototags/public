# GoToTags

A public project for:

- Feature requests and bugs for [GoToTags software](https://gototags.com/software)
- Technical support for [GoToTags software](https://gototags.com/software)
- Technical support for [GoToTags Store products](https://store.gototags.com)
- Art templates for [GoToTags Store custom printed tags](https://store.gototags.com/custom/)
- Encoding file templates for [GoToTags Store Tag Encoding Service](https://store.gototags.com/encoding/)
- 3rd party documentation, drivers, firmware and other tools for hardware and tags 

## Products

### Software 

The following are active GoToTags products:

- [GoToTags Cloud](https://gototags.com/cloud)
- [GoToTags Desktop App](https://learn.gototags.com/desktop-app)
- [GoToTags Encoder](https://gototags.com/encoder)
- [GoToTags iPhone App](https://gototags.com/iphone-app)

### Tags and Hardware

Barcode, NFC and UHF RFID tags and hardware can be purchased from the [GoToTags Store](https://store.gototags.com). 

GoToTags software does not require you to use tags and hardware purchased from the GoToTags Store, and conversely you do not have to use GoToTags software with tags and hardware purchased from the GoToTags Store. 

## Documentation

### GoToTags Software Documentation

Documentation for GoToTags software and related Connected Things technologies is availble on the [GoToTags Learn](https://learn.gototags.com/) website. Not everything is documented yet as it's a large and endless project, but it's actively being improved and maintained. Let us know if something is missing or needs improvement.

### Documentation for Products

3rd party documentation, drivers, firmware and other tools is hosted and maintained in this GitLab project to serve as a central point of access. These are not GoToTags documents and GoToTags is not responsible for their contents.

## Features, Bugs and Technical Support

The intention is for the GoToTags user community to be able to participate in the future product roadmap of GoToTags products and to identify any issues. Not all issues are tracked in this project; most are tracked in internal projects and linked to these public issues. 

[GitLab issues](https://docs.gitlab.com/ee/user/project/issues/) are used in this project to manage feature requests, bugs and questions. You will need a [GitLab account](https://gitlab.com/users/sign_in) to open an issue. Before opening an issue, look through the existing issues to see if a similar issue already exists which you can add on to. Make sure to provide as much detail (no private or personal data) as possible so that your requested change is clear. 

If you are new to bug reporting, read this external ["How to Write a Bug Report"](https://marker.io/blog/how-to-write-bug-report) article.

General technical or software development questions not relating to GoToTags products is best handled on [Stack Overflow](https://stackoverflow.com/).

## Open an Issue

To [open a GitLab issue](https://www.youtube.com/watch?v=iGNVJ3D0XnI) for a feature request, bug or question follow these steps:

1. Search [existing issues](https://gitlab.com/gototags/gototags/-/issues) to see if your issue has been reported or not
    - Look at `closed` issues as the issue might have already been resolved or implemented
    - If an existing issue has been found, add additional information if you have it and/or upvote it
2. If an existing issue has not been found, create a new Issue
3. Ensure you have a [GitLab account](https://gitlab.com/users/sign_in) (this website)
4. In this GitLab project, click `Issues` in the left nav
5. Click the `New Issue` button
6. Create a useful title for the issue
    - Include the incident id if relevant
7. For `Type`, choose the appropriate type ([Bug](https://gitlab.com/gototags/public/-/blob/main/.gitlab/issue_templates/Bug.md), [Feature](https://gitlab.com/gototags/public/-/blob/main/.gitlab/issue_templates/Feature.md), [Question](https://gitlab.com/gototags/public/-/blob/main/.gitlab/issue_templates/Question.md))
    - This will prepopulate the `Description` with a template for you to fill out
8. Fill out the `Description` field
    - Follow the template that was prepopulated
    - Provide as much detail as possible
    - Images are worth a thousand words (screenshots, ...)
9. Can leave `Assignees`, `Epic` and `Milestone` inputs blank
    - The GoToTags team will set these as appropriate
10. Chose `Labels` that are appropriate:
    - `desktop_app`, `bug`, (`nfc`, `uhf`, ...) 
11. Click the `Create issue` button

### Incident ID

Every incident in GoToTags software has a unique id (`incident.id`) that can be used to help identify the incident cause in the software. When searching for an issue in GitLab, try searching by the incident id.   

## Source Code

GoToTags software is closed commercial software, it is **not open source software**; the source code is not available. GoToTags also does not white label software.

## Security

If you have found what you believe to be a security issue; please [contact us](https://gototags.com/contact) immediately.      

## Code of Conduct

GoToTags reserves the right to modify and delete content and block users from this project in order to maintain a healthy user community.

## Contact

For additional questions; [contact us](https://gototags.com/contact).
